# vue-trivia

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

### Known bugs
* Answer radio buttons will appear as still clicked across questions, but if the button is not clicked again, correct answer will not count towards final score
* All correct answers are currently always the last option in the list of answers