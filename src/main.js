import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';

import Menu from './views/Menu.vue';
import GamePlay from './views/GamePlay.vue';
import GameOver from './views/GameOver.vue';
import VueSlider from 'vue-slider-component'
import 'vue-slider-component/theme/default.css'

Vue.component('VueSlider', VueSlider)
Vue.use(VueRouter);

Vue.config.productionTip = false;

const routes = [
  {
    path: '/',
    component: Menu
  },
  {
    path: '/game/:numberOfQs',
    name: 'game',
    component: GamePlay,
    props: true
  },
  {
    path: '/gameover/:answers:questions',
    name: 'gameover',
    component: GameOver,
    props: true
  }
];

const router = new VueRouter({
  routes
});

new Vue({
  router,
  components: {
    VueSlider: window['vue-slider-component']
  },
  render: h => h(App)
}).$mount('#app');
